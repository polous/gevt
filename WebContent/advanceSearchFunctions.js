var selectedSearchType = ""; 
var selectedDecisionType = "";
var cloneID = "";	//num

var requestQuery = "";
var enteredLat = "";
var enteredLon = "";
var enteredDistance = "";
var selectedCategory = "";
var eventName = "";
var dateFrom = null;
var dateTo = null;
var selectedDecision = "";

var addFlag = true;			// flag to handle removed sections when last entry is added after remove section is called
var colorFlag = true;
var sectionFlag = false;	//for disabling non-selected sections
var removeFlag = false;		//for checking whether an empty section has been removed or not

/*
 * showHideType(): Function to show or hide relevant divs
 * */
function showHideType(id){
	cloneID = id.slice(-1);
	if (isNaN(cloneID)){
		cloneID = "";
	}
	if(cloneID != ""){
		colorFlag = false;
	}else if(cloneID%2 == 0){
		colorFlag = true;		
	}else{
		colorFlag = false;
	}
	sectionFlag = true;
	var v1 = document.getElementById(id);
	var value1 = v1.options[v1.selectedIndex].value;
	if(value1 == 'LatLon'){
		document.getElementById('latlong'+cloneID).style.display = 'block';
	 	document.getElementById("category"+cloneID).style.display = 'none';
	 	document.getElementById("eventName"+cloneID).style.display = 'none';
	 	document.getElementById("duration"+cloneID).style.display = 'none';
	}
	if(value1 == 'Category'){
		document.getElementById('category'+cloneID).style.display = 'block';
	 	document.getElementById("latlong"+cloneID).style.display = 'none';
	 	document.getElementById("eventName"+cloneID).style.display = 'none';
	 	document.getElementById("duration"+cloneID).style.display = 'none';
	}
	if(value1 == 'Name of event'){
		document.getElementById('eventName'+cloneID).style.display = 'block';
	 	document.getElementById("latlong"+cloneID).style.display = 'none';
	 	document.getElementById("category"+cloneID).style.display = 'none';
	 	document.getElementById("duration"+cloneID).style.display = 'none';
	}		
	if(value1 == 'Duration'){
		document.getElementById('duration'+cloneID).style.display = 'block';
	 	document.getElementById("category"+cloneID).style.display = 'none';
	 	document.getElementById("eventName"+cloneID).style.display = 'none';
	 	document.getElementById("latlong"+cloneID).style.display = 'none';
	}	
}//END of showHideType()

/*
 * constructQuery(): Function to collect values from entries and constructing query
 * */
function constructQuery(){
	var decisionCheck = $('#selectdecision'+ cloneID).val();
	
	if(sectionFlag == true && (decisionCheck != "")){
		var selectionKey = 0;
		var temp = "";
		if(addFlag == true){
			var searchOption = document.getElementById("searchControls"+cloneID);
			selectedSearchType = searchOption.options[searchOption.selectedIndex].value;

			var dOption = document.getElementById("selectdecision"+cloneID);
			selectedDecision = dOption.options[dOption.selectedIndex].value;

			if(selectedSearchType == "LatLon"){ //1
				selectionKey = 1;
				enteredLat = document.getElementById("lat"+cloneID).value;
				enteredLon = document.getElementById("lon"+cloneID).value;
				var lOption = document.getElementById("distance"+cloneID);
				enteredDistance = lOption.options[lOption.selectedIndex].value;
				temp = enteredLat + "," + enteredLon + "," + enteredDistance;
			}if(selectedSearchType == "Category"){ //3
				selectionKey = 3;
				var cOption = document.getElementById("chooseCategory"+cloneID);
				selectedCategory = cOption.options[cOption.selectedIndex].value;
				temp = selectedCategory;
			}
			if(selectedSearchType == "Name of event"){ //2
				selectionKey = 2;
				eventName = document.getElementById("inputEventName"+cloneID).value;
				temp = eventName;
			}
			if(selectedSearchType == "Duration"){ //4
				selectionKey = 4;
				temp = dateFrom + "," + dateTo;
			}

			if(selectedDecision == "a"){
				requestQuery = requestQuery + selectionKey + "," + temp + "," + "a" +"|";
			}else if(selectedDecision == "o"){
				requestQuery = requestQuery + selectionKey + "," + temp + "," + "o" +"|";
			}else if(selectedDecision == "n"){
				requestQuery = requestQuery + selectionKey + "," + temp + "," + "n" +"|";
			}else if(selectedDecision == "none"){
				requestQuery = requestQuery + selectionKey + "," + temp + "," + "none" +"|";
			}
		}
		
		$('#latlong'+ cloneID+ ' :input').attr('disabled', true);
		$('#category'+ cloneID+ ' :input').attr('disabled', true);
		$('#eventName'+ cloneID+ ' :input').attr('disabled', true);
		$('#duration'+ cloneID+ ' :input').attr('disabled', true);

		$('#searchControls'+ cloneID).attr('disabled', true);
		$('#selectdecision'+ cloneID).attr('disabled', true);

		sectionFlag = false;
	}//END of outer IF
	else{
		alert("Kindly visit the Help Section for proper guidance.");
	}
}// END of constructQuery

/*
 * POST function to the server.
 * */
$( document ).ready(function() {
	$("#submitBtn").click(function() {
		
		constructQuery();
		//alert("requestQuery = " + requestQuery);
		
		if(requestQuery == ""){
			alert("There is no selection, Kindly visit the Help Section for proper guidance.");
		}else{
			var checkString = requestQuery.slice(-2).split("|").toString();
			checkString = checkString.replace(',', "");	

			if(checkString == "a" || checkString == "o"){
				alert("Note: You cannot select this decision in last proposition. Kindly visit the Help Section for proper guidance.");
			}else{
				$.ajax({
					type: "POST",
					url: "EventFinder",
					dataType: "json",
					data: "requestQuery="+requestQuery,
					cache: false,

					success: function(responseObj){
						var arr = new Array();
						var parseObj = JSON.stringify(responseObj);
						arr = JSON.parse(parseObj);
						var latt, lonn, catg;
						for (var i=0; i<arr.length; i++){
							latt = arr[i].latitude;
							lonn = arr[i].longitude;
							catg = arr[i].category;
							
							//var labelmarker= "Category: " + arr[i].category + " SubCategory: "+ arr[i].subcategory +" Number of Participants: "+arr[i].noOfParticipants;
							var labelid=arr[i].eventName;
							var labelmarker="";
							labelmarker = "<b><u style=\"color:grey\">" + labelid + "</u></b> <br>";
							if(arr[i].category)
								{
									labelmarker=labelmarker+"<b>Category:</b>"+arr[i].category+"<br>";
								}
							if(arr[i].subcategory)
								{
									labelmarker=labelmarker+"<b>Subcategory:</b>"+arr[i].subcategory+"<br>";
								}
							if(arr[i].noOfParticipants)
							{
								labelmarker=labelmarker+"<b>No of Participants:</b>"+arr[i].noOfParticipants+"<br>";
							}
							if(arr[i].url)
							{
								//labelmarker=labelmarker+"<b>URL:</b>"+"<href>"arr[i].url+"<br>";
								if(!(arr[i].url.toString().includes("http:")) && !(arr[i].url.toString().includes("https:")))
									{
										labelmarker=labelmarker+"<a href="+'\"'+ "http://"+arr[i].url+'\"'+ "target=\"_blank\">" +"Event URL</a>"; 
									}
								else{
									labelmarker=labelmarker+"<a href="+'\"'+ arr[i].url+'\"'+ "target=\"_blank\">" +"Event URL</a>"; 
									}
							}
									
								
							addmarker(latt/10000000, lonn/10000000, catg, labelmarker,labelid);
						}
						
						if(arr.length == 0){
							alert("No result found for your selection.");
						}
					},
					error: function(){
						alert('Server failure in retrieving search data!');
					}
				});				
			}
		}
	});
});//END of POST function

/*
 * removeSection(): For removing the section from the panel
 * */
function removeSection(){
	debugger;
	var tempCloneID;									//For checking if added section is empty or not
	if(cloneID == ""){
		tempCloneID = 2;
	}else if(removeFlag == false){
		tempCloneID = Number(cloneID) + 1;				//For the section which is empty
	}else{
		tempCloneID = Number(cloneID);					//For the section which is not empty
	}
	
	if($("#searchControls" + tempCloneID).val() != ""){
		if(typeof $("#searchControls" + tempCloneID).val() != "undefined"){
			removeFlag = false;
			constructQuery();
			cloneID = cloneID - 1;
			if(cloneID == 1){
				cloneID = "";
			}
			var removeStr = "";
			var removeString = requestQuery.split("\|");
			removeString[removeString.length-2] = "";
			removeStr = removeString.join("\|");
			requestQuery = removeStr.substring(0, removeStr.length-1);
		}
	}else{
		removeFlag = true;
		addFlag = true;
	}
}//END of removeSection()

/*
 * disableAddMore(): When NOT or NONE is selected, addition of more section is being disabled
 * */
function disableAddMore(){	
	var v = document.getElementById("selectdecision"+cloneID);
	if(v.options[v.selectedIndex].value == 'n' || v.options[v.selectedIndex].value == 'none'){
		document.getElementById("btnAdd").disabled = true;
	}else{
		document.getElementById("btnAdd").disabled = false;
	}
}//END of disableAddMore()

function getFromSearchDate(yearFrom, monthFrom, date){
	dateFrom = yearFrom+"-"+monthFrom+"-"+date;
}

function getToSearchDate(yearTo, monthTo, date){
	dateTo = yearTo+"-"+monthTo+"-"+date;
}